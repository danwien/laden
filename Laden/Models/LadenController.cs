﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;


namespace Laden.Models
{
    public class LadenController : ApiController
    {
        public static List<Product> Products = new List<Product>()
        {
            new Product {Id = 1, Name = "Kaese", Amount = 256, Price = 3 },
            new Product {Id = 2, Name = "Voll", Amount = 512, Price = 7 },
            new Product {Id = 3, Name = "Milch", Amount = 64, Price = 5 },
            new Product {Id = 4, Name = "Ausserdem", Amount = 128, Price = 6 },
            new Product {Id = 5, Name = "Vergangen", Amount = 1024, Price = 9 },
        };

        public int Amount { get; private set; }
        public int Id { get; private set; }

        // GET: api/Laden
        public IEnumerable<Product> Get()
        {
            return Products;
        }

        // GET: api/Laden/5
        public Product Get(int id)
        {
            foreach (var product in Products)
            {
                if(product.Id == id)
                {
                    return product;
                }
            }
            return null;
        }
        public void Buy()
        {
            var items = new List<PurchaseItem>
            {
                new PurchaseItem{ Id = 4, Amount = 3 },
                new PurchaseItem { Id = 2, Amount = 1 }
            };

            foreach (var product in Products)
            {
                foreach(var purchase in items)
                {
                    if(purchase.Id == purchase.Id && product.Amount >= purchase.Amount)
                    {
                        product.Amount = product.Amount - purchase.Amount;
                        break;
                    }
                }
            }
        }

        [HttpPost]
        [Route("api/Laden/buy")]
        public List<Product> Sort()
        {
            var result = new List<Product>();
            //Insertion, Selection
            return result;
        }
        // POST: api/Laden
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Laden/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Laden/5
        public void Delete(int id)
        {
        }
    }
}
